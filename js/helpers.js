// Used to get query parameters from the url
function GetURLParameter(searchValue) {
    var url = window.location.search.substring(1);
    // Split values
    var urlVariables = url.split('&');
    for (var i = 0; i < urlVariables.length; i++) {
        // Split values to get name and variable
        var variableName = urlVariables[i].split('=');
        // Check if this is the variable
        if (variableName[0] == searchValue) {
            if (searchValue == "time") {
                variableName[1] = variableName[1].replace("%3A", ":");
            } else if (searchValue == "email") {
                variableName[1] = variableName[1].replace("%40", "@");
            }
            return variableName[1];
        }
    }
}

// Write out the value and a line break
function WriteAndBreak(value) {
    document.write(value);
    document.write("<br>");
}

// Write a hidden input field the value
function WriteHiddenInput(value, fieldname) {
    document.write("<input type=\"hidden\" value=\"" + value + "\" name=\"" + fieldname + "\">");
}

// Write out the given post
function WritePost(post, date, user) {
    document.write("<tr><td>" + post + "</td><td>" + date + "</td><td>" + user + "</td></tr>");
}